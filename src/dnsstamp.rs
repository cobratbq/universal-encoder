use std::{
    io::{Error, ErrorKind, Read, Write},
    net::IpAddr,
};

use dns_stamp_parser::{Addr, DnsStamp, Props};

use crate::base16;

pub fn process(
    decoding: bool,
    reader: &mut dyn Read,
    writer: &mut dyn Write,
) -> Result<usize, Error> {
    if decoding {
        decode(reader, writer)
    } else {
        todo!("Implement DNSStamp encoder.")
    }
}

const DNSSTAMP_PREFIX: &str = "sdns://";

pub fn encode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    todo!("Implement DNSStamp encoder.")
}

pub fn decode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut buffer: Vec<u8> = Vec::new();
    reader.read_to_end(&mut buffer)?;
    if !buffer.starts_with(DNSSTAMP_PREFIX.as_bytes()) {
        return Err(Error::from(ErrorKind::InvalidInput));
    }
    let dnsstamp = DnsStamp::decode(String::from_utf8(buffer).unwrap().as_str()).unwrap();
    match dnsstamp {
        DnsStamp::DnsPlain(record) => {
            print!("DNS: ({})\n", format_props(record.props));
            print!("- address: {addr}\n", addr = record.addr);
        }
        DnsStamp::DnsCrypt(record) => {
            print!("DNSCrypt: {:?}\n", record);
            // FIXME: implement proper formatting, but requires fields to be exported.
        }
        DnsStamp::AnonymizedDnsCryptRelay(record) => {
            print!("Anonymized DNSCrypt relay:\n");
            print!("- address: {}\n", record.addr);
        }
        DnsStamp::DnsOverTls(record) => {
            print!("DNS-over-TLS: ({})\n", format_props(record.props));
            print!(
                "- address: {} ({})\n",
                record.hostname,
                record.addr.map_or(String::new(), format_socketaddr)
            );
            print!(
                "- bootstrap: {}\n",
                format_bootstrap_addresses(&record.bootstrap_ipi)
            );
            print!(
                "- public keys (sha256-hash): {}\n",
                format_sha256_hash(&record.hashi)
            );
            // FIXME: continue here
        }
        DnsStamp::DnsOverHttps(record) => {
            print!("DNS-over-HTTPS: ({})\n", format_props(record.props));
            print!(
                "- address: {} ({})\n",
                format_url(record.hostname, record.path),
                record.addr.map_or(String::new(), format_socketaddr)
            );
            print!(
                "- bootstrap: {}\n",
                format_bootstrap_addresses(&record.bootstrap_ipi)
            );
            print!(
                "- public keys (sha256-hash): {}\n",
                format_sha256_hash(&record.hashi)
            );
            // FIXME: continue here
        }
        DnsStamp::DnsOverQuic(record) => {
            print!("DNS-over-QUIC: ({})\n", format_props(record.props));
            print!(
                "- address: {} ({})\n",
                record.hostname,
                record.addr.map_or(String::new(), format_socketaddr)
            );
            print!(
                "- bootstrap: {}\n",
                format_bootstrap_addresses(&record.bootstrap_ipi)
            );
            print!(
                "- public keys (sha256-hash): {}\n",
                format_sha256_hash(&record.hashi)
            );
            // FIXME: continue here
        }
        DnsStamp::ObliviousDoHTarget(record) => {
            print!(
                "Oblivious DNS-over-HTTPS target: ({})\n",
                format_props(record.props)
            );
            print!("- address: {}\n", format_url(record.hostname, record.path));
        }
        DnsStamp::ObliviousDoHRelay(record) => {
            print!(
                "Oblivious DNS-over-HTTPS relay: ({})\n",
                format_props(record.props)
            );
            // FIXME: continue here
            print!(
                "- address: {} ({})\n",
                record.hostname,
                record.addr.map_or(String::new(), format_socketaddr)
            );
            print!(
                "- bootstrap: {}\n",
                format_bootstrap_addresses(&record.bootstrap_ipi)
            );
            print!(
                "- public keys (sha256-hash): {}\n",
                format_sha256_hash(&record.hashi)
            );
            // FIXME: continue here
        }
    }
    Ok(0)
}

fn format_url(addr: String, path: String) -> String {
    format!(
        "https://{}{}",
        addr,
        if path.starts_with("/") {
            path
        } else {
            format!("/{}", path)
        }
    )
}

fn format_props(props: Props) -> String {
    let mut descs = Vec::<&str>::new();
    if props & Props::DNSSEC == Props::DNSSEC {
        descs.push("DNSSEC");
    }
    if props & Props::NO_LOGS == Props::NO_LOGS {
        descs.push("no logs");
    }
    if props & Props::NO_FILTER == Props::NO_FILTER {
        descs.push("no filtering");
    }
    descs.join(", ")
}

fn format_bootstrap_addresses(addrs: &Vec<IpAddr>) -> String {
    addrs
        .iter()
        .map(|a| a.to_string())
        .fold(String::new(), |cur, a| {
            if cur.is_empty() {
                a.to_string()
            } else {
                format!("{}, {}", cur, a.to_string())
            }
        })
}

fn format_sha256_hash(hashes: &Vec<[u8; 32]>) -> String {
    let mut formatted = Vec::<String>::new();
    for hash in hashes {
        formatted
            .push(String::from_utf8(encode_base16(hash)).expect("illegal hash value encountered"));
    }
    formatted.join(" ")
}

fn encode_base16(input: &[u8]) -> Vec<u8> {
    let mut output = Vec::new();
    base16::encode(&mut input.clone(), &mut output).expect("failed to encode bytes");
    output
}

fn format_socketaddr(addr: dns_stamp_parser::Addr) -> String {
    match addr {
        Addr::SocketAddr(sockaddr) => {
            format!("{}", sockaddr.to_string())
        }
        Addr::Port(port) => {
            format!(":{}", port)
        }
    }
}

#[cfg(test)]
mod tests {}
