use std::{io::{Error, ErrorKind, Read, Write}};

// Bencoding: https://www.bittorrent.org/beps/bep_0003.html

pub fn process(
    decoding: bool,
    reader: &mut dyn Read,
    writer: &mut dyn Write,
) -> Result<usize, Error> {
    if decoding {
        decode(reader, writer)
    } else {
        Err(Error::from(ErrorKind::Unsupported))
    }
}

pub fn decode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    panic!("To be implemented.")
}

fn bdecode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut t: [u8; 1];
    let result = reader.read(&mut t)?;
    if result == 0 {
        return Err(Error::from(ErrorKind::UnexpectedEof));
    }
    match t[0] {
        b'0' | b'1' | b'2' | b'3' | b'4' | b'5' | b'6' | b'7' | b'8' | b'9' => {
            // FIXME arbitrary (low) value for capacity.
            let mut length: Vec<u8> = Vec::with_capacity(10);
            length.push(t[0]);
            read_number(&mut length, reader, b':')?;
        }
        b'i' => {
            // FIXME arbitrary (low) value for capacity.
            let integer_bytes = Vec::with_capacity(10);
            let val = read_number(&mut integer_bytes, &mut reader, b'e')?;
            writer.write_fmt(format_args!("{}", &val))?;
            // FIXME need to find real written length.
            Ok(0usize)
        },
        b'l' => {},
        b'd' => {},
        _ => Err(Error::from(ErrorKind::InvalidData)),
    }
}

fn read_string(reader: &mut dyn Read) -> Result<Vec<u8>, Error> {
    // FIXME arbitrary capacity chosen.
    let length_buf = Vec::with_capacity(10);
    read_number(&mut length_buf, &mut reader, b':')?;
    let content = Vec::with_capacity(length);
    let n = reader.read(&mut content)?;
    if n < content.len() {
        return Err(Error::from(ErrorKind::UnexpectedEof));
    }
    Ok(content)
}

fn read_number(dest: &mut Vec<u8>, reader: &mut dyn Read, sep: u8) -> Result<(), Error> {
    let offset = dest.len();
    let mut d: [u8;1];
    while dest.len() < dest.capacity() {
        let l = reader.read(&mut d)?;
        if l == 0 {
            return Err(Error::from(ErrorKind::UnexpectedEof))
        }
        if d[0] == sep {
            return Ok(())
        }
        dest.push(d[0]);
    }
    Err(Error::from(ErrorKind::OutOfMemory))
}