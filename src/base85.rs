use std::io::{Error, Read, Write};

pub fn process(
    decoding: bool,
    reader: &mut dyn Read,
    writer: &mut dyn Write,
) -> Result<usize, Error> {
    if decoding {
        decode(reader, writer)
    } else {
        encode(reader, writer)
    }
}

pub fn encode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut buffer: [u8; 4] = [0; 4];
    let mut output: [u8; 5] = [0; 5];
    let mut size: usize = 0;
    loop {
        let n = reader.read(&mut buffer)?;
        size += match n {
            4 => {
                let x: u32 = u32::from_be_bytes(buffer);
                if x == 0 {
                    writer.write(&[b'z'])?
                } else {
                    output[0] = ((x / 52200625) % 85) as u8 + 33;
                    output[1] = ((x / 614125) % 85) as u8 + 33;
                    output[2] = ((x / 7225) % 85) as u8 + 33;
                    output[3] = ((x / 85) % 85) as u8 + 33;
                    output[4] = (x % 85) as u8 + 33;
                    writer.write(&output)?
                }
            }
            3 => {
                buffer[3] = 0;
                let x: u32 = u32::from_be_bytes(buffer);
                output[0] = ((x / 52200625) % 85) as u8 + 33;
                output[1] = ((x / 614125) % 85) as u8 + 33;
                output[2] = ((x / 7225) % 85) as u8 + 33;
                output[3] = ((x / 85) % 85) as u8 + 33;
                writer.write(&output[..4])?
            }
            2 => {
                buffer[2] = 0;
                buffer[3] = 0;
                let x: u32 = u32::from_be_bytes(buffer);
                output[0] = ((x / 52200625) % 85) as u8 + 33;
                output[1] = ((x / 614125) % 85) as u8 + 33;
                output[2] = ((x / 7225) % 85) as u8 + 33;
                writer.write(&output[..3])?
            }
            1 => {
                buffer[1] = 0;
                buffer[2] = 0;
                buffer[3] = 0;
                let x: u32 = u32::from_be_bytes(buffer);
                output[0] = ((x / 52200625) % 85) as u8 + 33;
                output[1] = ((x / 614125) % 85) as u8 + 33;
                writer.write(&output[..2])?
            }
            0 => break,
            _ => panic!("Illegal number of bytes encountered."),
        };
    }
    Ok(size)
}

pub fn decode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut buffer: [u8; 5] = [0; 5];
    let mut size = 0usize;
    loop {
        let n = reader.read(&mut buffer)?;
        size += match n {
            5 => {
                let mut u = (buffer[0] - 33) as u32 * 52200625;
                u += (buffer[1] - 33) as u32 * 614125;
                u += (buffer[2] - 33) as u32 * 7225;
                u += (buffer[3] - 33) as u32 * 85;
                u += (buffer[4] - 33) as u32;
                let output = u.to_be_bytes();
                writer.write(&output)?
            }
            4 => {
                buffer[4] = b'u';
                let mut u = (buffer[0] - 33) as u32 * 52200625;
                u += (buffer[1] - 33) as u32 * 614125;
                u += (buffer[2] - 33) as u32 * 7225;
                u += (buffer[3] - 33) as u32 * 85;
                u += (buffer[4] - 33) as u32;
                let output = u.to_be_bytes();
                writer.write(&output[..3])?
            }
            3 => {
                buffer[3] = b'u';
                buffer[4] = b'u';
                let mut u = (buffer[0] - 33) as u32 * 52200625;
                u += (buffer[1] - 33) as u32 * 614125;
                u += (buffer[2] - 33) as u32 * 7225;
                u += (buffer[3] - 33) as u32 * 85;
                u += (buffer[4] - 33) as u32;
                let output = u.to_be_bytes();
                writer.write(&output[..2])?
            }
            2 => {
                buffer[2] = b'u';
                buffer[3] = b'u';
                buffer[4] = b'u';
                let mut u = (buffer[0] - 33) as u32 * 52200625;
                u += (buffer[1] - 33) as u32 * 614125;
                u += (buffer[2] - 33) as u32 * 7225;
                u += (buffer[3] - 33) as u32 * 85;
                u += (buffer[4] - 33) as u32;
                let output = u.to_be_bytes();
                writer.write(&output[..1])?
            }
            0 => break,
            _ => panic!("Illegal number of bytes encountered."),
        };
    }
    Ok(size)
}

#[cfg(test)]
mod tests {

    use super::{decode, encode};

    #[test]
    fn test_base85_encode() {
        let expected = "9jqo^BlbD-BleB1DJ+*+F(f,q/0JhKF<GL>Cj@.4Gp$d7F!,L7@<6@)/0JDEF<G%<+EV:2F!,O<DJ+*.@<*K0@<6L(Df-\\0Ec5e;DffZ(EZee.Bl.9pF\"AGXBPCsi+DGm>@3BB/F*&OCAfu2/AKYi(DIb:@FD,*)+C]U=@3BN#EcYf8ATD3s@q?d$AftVqCh[NqF<G:8+EV:.+Cf>-FD5W8ARlolDIal(DId<j@<?3r@:F%a+D58'ATD4$Bl@l3De:,-DJs`8ARoFb/0JMK@qB4^F!,R<AKZ&-DfTqBG%G>uD.RTpAKYo'+CT/5+Cei#DII?(E,9)oF*2M7/c";
        let input = b"Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.";
        let mut output = vec![0; 0];
        let result = encode(&mut &input[..], &mut output).unwrap();
        assert_eq!(expected.as_bytes(), output.as_slice());
        assert_eq!(expected.len(), result);
    }

    #[test]
    fn test_base85_decode() {
        let expected = "Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.";
        let input: &[u8] = b"9jqo^BlbD-BleB1DJ+*+F(f,q/0JhKF<GL>Cj@.4Gp$d7F!,L7@<6@)/0JDEF<G%<+EV:2F!,O<DJ+*.@<*K0@<6L(Df-\\0Ec5e;DffZ(EZee.Bl.9pF\"AGXBPCsi+DGm>@3BB/F*&OCAfu2/AKYi(DIb:@FD,*)+C]U=@3BN#EcYf8ATD3s@q?d$AftVqCh[NqF<G:8+EV:.+Cf>-FD5W8ARlolDIal(DId<j@<?3r@:F%a+D58'ATD4$Bl@l3De:,-DJs`8ARoFb/0JMK@qB4^F!,R<AKZ&-DfTqBG%G>uD.RTpAKYo'+CT/5+Cei#DII?(E,9)oF*2M7/c";
        let mut output = vec![0; 0];
        let result = decode(&mut &input[..], &mut output).unwrap();
        assert_eq!(expected.as_bytes(), output.as_slice());
        assert_eq!(expected.len(), result);
    }
}
