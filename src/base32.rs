use std::io::{Error, ErrorKind, Read, Write};

pub fn process(
    decoding: bool,
    reader: &mut dyn Read,
    writer: &mut dyn Write,
) -> Result<usize, Error> {
    if decoding {
        decode(reader, writer)
    } else {
        encode(reader, writer)
    }
}

pub const BASE32_LOOKUP_TABLE: &[u8; 32] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";

// TODO support variant z-Base-32.
// TODO support variant Crockford's Base32.
// TODO support variant base32hex ("Extended Hex")
pub fn encode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut buffer: [u8; 5] = [0; 5];
    let mut output: [u8; 8] = [0; 8];
    let mut size: usize = 0;
    loop {
        let n = reader.read(&mut buffer)?;
        match n {
            5 => {
                output[0] = BASE32_LOOKUP_TABLE[((0b11111000 & buffer[0]) >> 3) as usize];
                output[1] = BASE32_LOOKUP_TABLE
                    [(((0b00000111 & buffer[0]) << 2) | ((0b11000000 & buffer[1]) >> 6)) as usize];
                output[2] = BASE32_LOOKUP_TABLE[((0b00111110 & buffer[1]) >> 1) as usize];
                output[3] = BASE32_LOOKUP_TABLE
                    [(((0b00000001 & buffer[1]) << 4) | ((0b11110000 & buffer[2]) >> 4)) as usize];
                output[4] = BASE32_LOOKUP_TABLE
                    [(((0b00001111 & buffer[2]) << 1) | ((0b10000000 & buffer[3]) >> 7)) as usize];
                output[5] = BASE32_LOOKUP_TABLE[((0b01111100 & buffer[3]) >> 2) as usize];
                output[6] = BASE32_LOOKUP_TABLE
                    [(((0b00000011 & buffer[3]) << 3) | ((0b11100000 & buffer[4]) >> 5)) as usize];
                output[7] = BASE32_LOOKUP_TABLE[(0b00011111 & buffer[4]) as usize];
            }
            4 => {
                output[0] = BASE32_LOOKUP_TABLE[((0b11111000 & buffer[0]) >> 3) as usize];
                output[1] = BASE32_LOOKUP_TABLE
                    [(((0b00000111 & buffer[0]) << 2) | ((0b11000000 & buffer[1]) >> 6)) as usize];
                output[2] = BASE32_LOOKUP_TABLE[((0b00111110 & buffer[1]) >> 1) as usize];
                output[3] = BASE32_LOOKUP_TABLE
                    [(((0b00000001 & buffer[1]) << 4) | ((0b11110000 & buffer[2]) >> 4)) as usize];
                output[4] = BASE32_LOOKUP_TABLE
                    [(((0b00001111 & buffer[2]) << 1) | ((0b10000000 & buffer[3]) >> 7)) as usize];
                output[5] = BASE32_LOOKUP_TABLE[((0b01111100 & buffer[3]) >> 2) as usize];
                output[6] = BASE32_LOOKUP_TABLE[((0b00000011 & buffer[3]) << 3) as usize];
                output[7] = b'=';
            }
            3 => {
                output[0] = BASE32_LOOKUP_TABLE[((0b11111000 & buffer[0]) >> 3) as usize];
                output[1] = BASE32_LOOKUP_TABLE
                    [(((0b00000111 & buffer[0]) << 2) | ((0b11000000 & buffer[1]) >> 6)) as usize];
                output[2] = BASE32_LOOKUP_TABLE[((0b00111110 & buffer[1]) >> 1) as usize];
                output[3] = BASE32_LOOKUP_TABLE
                    [(((0b00000001 & buffer[1]) << 4) | ((0b11110000 & buffer[2]) >> 4)) as usize];
                output[4] = BASE32_LOOKUP_TABLE[((0b00001111 & buffer[2]) << 1) as usize];
                output[5] = b'=';
                output[6] = b'=';
                output[7] = b'=';
            }
            2 => {
                output[0] = BASE32_LOOKUP_TABLE[((0b11111000 & buffer[0]) >> 3) as usize];
                output[1] = BASE32_LOOKUP_TABLE
                    [(((0b00000111 & buffer[0]) << 2) | ((0b11000000 & buffer[1]) >> 6)) as usize];
                output[2] = BASE32_LOOKUP_TABLE[((0b00111110 & buffer[1]) >> 1) as usize];
                output[3] = BASE32_LOOKUP_TABLE[((0b00000001 & buffer[1]) << 4) as usize];
                output[4] = b'=';
                output[5] = b'=';
                output[6] = b'=';
                output[7] = b'=';
            }
            1 => {
                output[0] = BASE32_LOOKUP_TABLE[((0b11111000 & buffer[0]) >> 3) as usize];
                output[1] = BASE32_LOOKUP_TABLE[((0b00000111 & buffer[0]) << 2) as usize];
                output[2] = b'=';
                output[3] = b'=';
                output[4] = b'=';
                output[5] = b'=';
                output[6] = b'=';
                output[7] = b'=';
            }
            0 => break,
            _ => panic!("Illegal number of bytes encountered."),
        }
        size += writer.write(&output)?;
    }
    debug_assert!(size % 8 == 0);
    Ok(size)
}

pub fn decode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut buffer: [u8; 8] = [0; 8];
    let mut output: Vec<u8> = Vec::new();
    let mut size: usize = 0;
    loop {
        size += writer.write(&output)?;
        output.clear();
        let n = reader.read(&mut buffer)?;
        let mut v: u8;
        match n {
            8 => {
                match lookup_value(buffer[0]) {
                    Some(c) => v = c << 3,
                    None => return Err(Error::from(ErrorKind::InvalidInput)),
                }
                match lookup_value(buffer[1]) {
                    Some(c) => {
                        v |= (c & 0b00011100) >> 2;
                        output.push(v);
                        v = (c & 0b00000011) << 6;
                    }
                    None => continue,
                }
                match lookup_value(buffer[2]) {
                    Some(c) => {
                        v |= (c & 0b00011111) << 1;
                    }
                    None => continue,
                }
                match lookup_value(buffer[3]) {
                    Some(c) => {
                        v |= (c & 0b00010000) >> 4;
                        output.push(v);
                        v = (c & 0b00001111) << 4;
                    }
                    None => continue,
                }
                match lookup_value(buffer[4]) {
                    Some(c) => {
                        v |= (c & 0b00011110) >> 1;
                        output.push(v);
                        v = (c & 0b00000001) << 7;
                    }
                    None => continue,
                }
                match lookup_value(buffer[5]) {
                    Some(c) => v |= (c & 0b00011111) << 2,
                    None => continue,
                }
                match lookup_value(buffer[6]) {
                    Some(c) => {
                        v |= (c & 0b00011000) >> 3;
                        output.push(v);
                        v = (c & 0b00000111) << 5;
                    }
                    None => continue,
                }
                match lookup_value(buffer[7]) {
                    Some(c) => {
                        v |= c & 0b00011111;
                        output.push(v);
                    }
                    None => continue,
                }
            }
            7 | 6 | 5 | 4 | 3 | 2 | 1 => return Err(Error::from(ErrorKind::InvalidData)),
            0 => break,
            _ => panic!("Illegal number of bytes encountered."),
        }
    }
    Ok(size)
}

fn lookup_value(c: u8) -> Option<u8> {
    match c {
        b'A' => Some(0),
        b'B' => Some(1),
        b'C' => Some(2),
        b'D' => Some(3),
        b'E' => Some(4),
        b'F' => Some(5),
        b'G' => Some(6),
        b'H' => Some(7),
        b'I' => Some(8),
        b'J' => Some(9),
        b'K' => Some(10),
        b'L' => Some(11),
        b'M' => Some(12),
        b'N' => Some(13),
        b'O' => Some(14),
        b'P' => Some(15),
        b'Q' => Some(16),
        b'R' => Some(17),
        b'S' => Some(18),
        b'T' => Some(19),
        b'U' => Some(20),
        b'V' => Some(21),
        b'W' => Some(22),
        b'X' => Some(23),
        b'Y' => Some(24),
        b'Z' => Some(25),
        b'2' => Some(26),
        b'3' => Some(27),
        b'4' => Some(28),
        b'5' => Some(29),
        b'6' => Some(30),
        b'7' => Some(31),
        b'=' => None,
        _ => panic!("Illegal character."),
    }
}

#[cfg(test)]
mod tests {

    use super::{decode, encode};

    #[test]
    fn test_base32_encode_zero_length_string() {
        let expected: [u8; 0] = [];
        let mut input: &[u8] = &[0; 0];
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_decode_zero_length_string() {
        let expected: [u8; 0] = [];
        let mut input: &[u8] = &[0; 0];
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_encode_foobar_testvector() {
        let expected: &[u8; 16] = b"MZXW6YTBOI======";
        let mut input: &[u8] = b"foobar";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_decode_foobar_testvector() {
        let expected: &[u8] = b"foobar";
        let mut input: &[u8] = b"MZXW6YTBOI======";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        println!("{:?}", output);
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_encode_fooba_testvector() {
        let expected: &[u8; 8] = b"MZXW6YTB";
        let mut input: &[u8] = b"fooba";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_decode_fooba_testvector() {
        let expected: &[u8] = b"fooba";
        let mut input: &[u8] = b"MZXW6YTB";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_encode_foob_testvector() {
        let expected: &[u8; 8] = b"MZXW6YQ=";
        let mut input: &[u8] = b"foob";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_decode_foob_testvector() {
        let expected: &[u8] = b"foob";
        let mut input: &[u8] = b"MZXW6YQ=";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_encode_foo_testvector() {
        let expected: &[u8; 8] = b"MZXW6===";
        let mut input: &[u8] = b"foo";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_decode_foo_testvector() {
        let expected: &[u8] = b"foo";
        let mut input: &[u8] = b"MZXW6===";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_encode_fo_testvector() {
        let expected: &[u8; 8] = b"MZXQ====";
        let mut input: &[u8] = b"fo";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_decode_fo_testvector() {
        let expected: &[u8] = b"fo";
        let mut input: &[u8] = b"MZXQ====";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_encode_f_testvector() {
        let expected: &[u8; 8] = b"MY======";
        let mut input: &[u8] = b"f";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base32_decode_f_testvector() {
        let expected: &[u8] = b"f";
        let mut input: &[u8] = b"MY======";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }
}
