use std::io::{Error, ErrorKind, Read, Write};

pub fn process(
    decoding: bool,
    reader: &mut dyn Read,
    writer: &mut dyn Write,
) -> Result<usize, Error> {
    if decoding {
        decode(reader, writer)
    } else {
        encode(reader, writer)
    }
}

pub const BASE16_LOOKUP_TABLE: &[u8; 16] = b"0123456789ABCDEF";

pub fn encode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut buffer: [u8; 1] = [0; 1];
    let mut output: [u8; 2] = [0; 2];
    let mut size: usize = 0;
    loop {
        let n = reader.read(&mut buffer)?;
        match n {
            1 => {
                output[0] = BASE16_LOOKUP_TABLE[((buffer[0] & 0xf0) >> 4) as usize];
                output[1] = BASE16_LOOKUP_TABLE[(buffer[0] & 0x0f) as usize];
            }
            0 => break,
            _ => panic!("Illegal number of bytes encountered."),
        }
        size += writer.write(&output)?;
    }
    debug_assert!(size % 2 == 0);
    Ok(size)
}

pub fn decode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut output: [u8; 1] = [0; 1];
    let mut buffer: [u8; 2] = [0; 2];
    let mut size: usize = 0;
    loop {
        let n = reader.read(&mut buffer)?;
        match n {
            2 => {
                output[0] = lookup_value(buffer[0]) << 4 | lookup_value(buffer[1]);
            }
            1 => return Err(Error::from(ErrorKind::InvalidData)),
            0 => break,
            _ => panic!("Illegal number of bytes encountered."),
        }
        size += writer.write(&output)?;
    }
    Ok(size)
}

fn lookup_value(c: u8) -> u8 {
    match c {
        b'0' => 0x0,
        b'1' => 0x1,
        b'2' => 0x2,
        b'3' => 0x3,
        b'4' => 0x4,
        b'5' => 0x5,
        b'6' => 0x6,
        b'7' => 0x7,
        b'8' => 0x8,
        b'9' => 0x9,
        b'a' | b'A' => 0xa,
        b'b' | b'B' => 0xb,
        b'c' | b'C' => 0xc,
        b'd' | b'D' => 0xd,
        b'e' | b'E' => 0xe,
        b'f' | b'F' => 0xf,
        _ => panic!("Illegal character: not a hexadecimal character"),
    }
}

#[cfg(test)]
mod tests {

    use super::{decode, encode};

    #[test]
    fn test_base16_encode_zero_length_string() {
        let expected: [u8; 0] = [];
        let mut input: &[u8] = &[0; 0];
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_ref());
    }

    #[test]
    fn test_base16_decode_zero_length_string() {
        let expected: &[u8] = &[];
        let mut input: &[u8] = &[];
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base16_encode_hello_world() {
        let expected: &[u8; 22] = b"48656C6C6F20776F726C64";
        let mut input: &[u8] = b"Hello world";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base16_decode_hello_world() {
        let expected: &[u8] = b"Hello world";
        let mut input: &[u8] = b"48656C6C6F20776F726C64";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base16_random_bytes_encode_decode() {
        let pid = std::process::id();
        let mut input: &[u8] = &u32::to_be_bytes(pid);
        let mut encoded: Vec<u8> = Vec::new();
        assert_eq!(8 as usize, encode(&mut input, &mut encoded).unwrap());
        let mut output: Vec<u8> = Vec::new();
        assert_eq!(4 as usize, decode(&mut &encoded[..], &mut output).unwrap());
        assert_eq!(
            pid,
            u32::from_be_bytes([output[0], output[1], output[2], output[3]])
        );
    }
}
