use std::{io::{Error, ErrorKind}, str::FromStr};
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
pub enum Operation {
    Decode,
    Encode,
}

#[derive(StructOpt, Debug)]
pub enum Endianness {
    BigEndian,
    LittleEndian,
}

impl FromStr for Endianness {
    type Err = Error;
    fn from_str(endianness: &str) -> Result<Self, Error> {
        match endianness {
            "big-endian" => Ok(Endianness::BigEndian),
            "little-endian" => Ok(Endianness::LittleEndian),
            _ => Err(Error::from(ErrorKind::InvalidInput)),
        }
    }
}

#[derive(StructOpt, Debug)]
pub enum Signedness {
    Signed,
    Unsigned,
}

impl FromStr for Signedness {
    type Err = Error;
    fn from_str(signedness: &str) -> Result<Self, Error> {
        match signedness {
            "signed" => Ok(Signedness::Signed),
            "unsigned" => Ok(Signedness::Unsigned),
            _ => Err(Error::from(ErrorKind::InvalidInput)),
        }
    }
}
