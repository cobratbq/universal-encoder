use std::io::{Error, ErrorKind, Read, Write};

pub fn process(
    decoding: bool,
    reader: &mut dyn Read,
    writer: &mut dyn Write,
) -> Result<usize, Error> {
    if decoding {
        decode(reader, writer)
    } else {
        encode(reader, writer)
    }
}

pub const BASE64_LOOKUP_TABLE: &[u8; 64] =
    b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/**
 * RFC 4648: Base64 encoding
 */
// TODO configurable w/ or w/o padding
// TODO support for Base64 MIME-variant: length-constraint + line-separator.
// TODO Radix-64 for OpenPGP (RFC 4880): length-constraint, line-separator, 24-bit checksum.
// TODO Base64-encoding for IMAP (RFC 3501): 63rd char.
// TODO Base64 URL-/filename-safe: 62nd char, 63rd char, optional padding.
pub fn encode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut buffer: [u8; 3] = [0; 3];
    let mut output: [u8; 4] = [0; 4];
    let mut size: usize = 0;
    loop {
        let n = reader.read(&mut buffer)?;
        match n {
            3 => {
                output[0] = BASE64_LOOKUP_TABLE[((0b11111100 & buffer[0]) >> 2) as usize];
                output[1] = BASE64_LOOKUP_TABLE
                    [(((0b00000011 & buffer[0]) << 4) | ((0b11110000 & buffer[1]) >> 4)) as usize];
                output[2] = BASE64_LOOKUP_TABLE
                    [(((0b00001111 & buffer[1]) << 2) | ((0b11000000 & buffer[2]) >> 6)) as usize];
                output[3] = BASE64_LOOKUP_TABLE[(0b00111111 & buffer[2]) as usize];
            }
            2 => {
                output[0] = BASE64_LOOKUP_TABLE[((0b11111100 & buffer[0]) >> 2) as usize];
                output[1] = BASE64_LOOKUP_TABLE
                    [(((0b00000011 & buffer[0]) << 4) | ((0b11110000 & buffer[1]) >> 4)) as usize];
                output[2] = BASE64_LOOKUP_TABLE[((0b00001111 & buffer[1]) << 2) as usize];
                output[3] = b'=';
            }
            1 => {
                output[0] = BASE64_LOOKUP_TABLE[((0b11111100 & buffer[0]) >> 2) as usize];
                output[1] = BASE64_LOOKUP_TABLE[((0b00000011 & buffer[0]) << 4) as usize];
                output[2] = b'=';
                output[3] = b'=';
            }
            0 => break,
            _ => panic!("Illegal number of bytes encountered."),
        }
        size += writer.write(&output)?;
    }
    debug_assert!(size % 4 == 0);
    Ok(size)
}

/**
 * RFC 4648: Base64 encoding
 */
pub fn decode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut buffer: [u8; 4] = [0; 4];
    let mut size: usize = 0;
    let mut output: Vec<u8> = Vec::new();
    loop {
        size += writer.write(&output)?;
        output.clear();
        let mut v: u8;
        let n = reader.read(&mut buffer)?;
        match n {
            4 => {
                match lookup_value(buffer[0]) {
                    Some(c) => v = c << 2,
                    None => return Err(Error::from(ErrorKind::InvalidInput)),
                }
                match lookup_value(buffer[1]) {
                    Some(c) => {
                        v |= (c >> 4) & 0b00000011;
                        output.push(v);
                        v = (c << 4) & 0b11110000;
                    }
                    None => continue,
                }
                match lookup_value(buffer[2]) {
                    Some(c) => {
                        v |= (c >> 2) & 0b00001111;
                        output.push(v);
                        v = (c << 6) & 0b11000000;
                    }
                    None => continue,
                }
                match lookup_value(buffer[3]) {
                    Some(c) => {
                        v |= c & 0b00111111;
                        println!("Testing... {:?}", v);
                        output.push(v);
                        println!("Output: {:?}", output);
                    }
                    None => continue,
                }
            }
            3 | 2 | 1 => return Err(Error::from(ErrorKind::InvalidInput)),
            0 => break,
            _ => panic!("Illegal number of bytes encountered."),
        }
    }
    Ok(size)
}

fn lookup_value(c: u8) -> Option<u8> {
    match c {
        b'A' => Some(0),
        b'B' => Some(1),
        b'C' => Some(2),
        b'D' => Some(3),
        b'F' => Some(5),
        b'E' => Some(4),
        b'G' => Some(6),
        b'H' => Some(7),
        b'I' => Some(8),
        b'J' => Some(9),
        b'K' => Some(10),
        b'L' => Some(11),
        b'M' => Some(12),
        b'N' => Some(13),
        b'O' => Some(14),
        b'P' => Some(15),
        b'Q' => Some(16),
        b'R' => Some(17),
        b'S' => Some(18),
        b'T' => Some(19),
        b'U' => Some(20),
        b'V' => Some(21),
        b'W' => Some(22),
        b'X' => Some(23),
        b'Y' => Some(24),
        b'Z' => Some(25),
        b'a' => Some(26),
        b'b' => Some(27),
        b'c' => Some(28),
        b'd' => Some(29),
        b'e' => Some(30),
        b'f' => Some(31),
        b'g' => Some(32),
        b'h' => Some(33),
        b'i' => Some(34),
        b'j' => Some(35),
        b'k' => Some(36),
        b'l' => Some(37),
        b'm' => Some(38),
        b'n' => Some(39),
        b'o' => Some(40),
        b'p' => Some(41),
        b'q' => Some(42),
        b'r' => Some(43),
        b's' => Some(44),
        b't' => Some(45),
        b'u' => Some(46),
        b'v' => Some(47),
        b'w' => Some(48),
        b'x' => Some(49),
        b'y' => Some(50),
        b'z' => Some(51),
        b'0' => Some(52),
        b'1' => Some(53),
        b'2' => Some(54),
        b'3' => Some(55),
        b'4' => Some(56),
        b'5' => Some(57),
        b'6' => Some(58),
        b'7' => Some(59),
        b'8' => Some(60),
        b'9' => Some(61),
        b'+' => Some(62),
        b'/' => Some(63),
        b'=' => None,
        _ => panic!("Illegal character for Base64 encoding encountered."),
    }
}

#[cfg(test)]
mod tests {

    use super::{decode, encode};

    #[test]
    fn test_base64_encode_zero_length_string() {
        let expected: [u8; 0] = [];
        let mut input: &[u8] = &[0; 0];
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_ref());
    }

    #[test]
    fn test_base64_decode_zero_length_string() {
        let expected: [u8; 0] = [];
        let mut input: &[u8] = &[0; 0];
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_ref());
    }

    #[test]
    fn test_base64_encode_hello_string() {
        let expected: &[u8; 8] = b"SGVsbG8=";
        let mut input: &[u8] = b"Hello";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_decode_hello_string() {
        let expected: &[u8] = b"Hello";
        let mut input: &[u8] = b"SGVsbG8=";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_encode_foobar_testvector() {
        let expected: &[u8; 8] = b"Zm9vYmFy";
        let mut input: &[u8] = b"foobar";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_decode_foobar_testvector() {
        let expected: &[u8] = b"foobar";
        let mut input: &[u8] = b"Zm9vYmFy";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_encode_fooba_testvector() {
        let expected: &[u8; 8] = b"Zm9vYmE=";
        let mut input: &[u8] = b"fooba";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_decode_fooba_testvector() {
        let expected: &[u8] = b"fooba";
        let mut input: &[u8] = b"Zm9vYmE=";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_encode_foob_testvector() {
        let expected: &[u8; 8] = b"Zm9vYg==";
        let mut input: &[u8] = b"foob";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_decode_foob_testvector() {
        let mut input: &[u8] = b"Zm9vYg==";
        let expected: &[u8] = b"foob";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_encode_foo_testvector() {
        let expected: &[u8; 4] = b"Zm9v";
        let mut input: &[u8] = b"foo";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_decode_foo_testvector() {
        let expected: &[u8] = b"foo";
        let mut input: &[u8] = b"Zm9v";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_encode_fo_testvector() {
        let expected: &[u8; 4] = b"Zm8=";
        let mut input: &[u8] = b"fo";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_decode_fo_testvector() {
        let expected: &[u8] = b"fo";
        let mut input: &[u8] = b"Zm8=";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_encode_f_testvector() {
        let expected: &[u8; 4] = b"Zg==";
        let mut input: &[u8] = b"f";
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_base64_decode_f_testvector() {
        let expected: &[u8] = b"f";
        let mut input: &[u8] = b"Zg==";
        let mut output = vec![0; 0];
        let result = decode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }
}
