use std::io::{Error, ErrorKind, Read, Write};

pub fn process(
    decoding: bool,
    reader: &mut dyn Read,
    writer: &mut dyn Write,
) -> Result<usize, Error> {
    if decoding {
        decode(reader, writer)
    } else {
        encode(reader, writer, b'1')
    }
}

const BASE58_LOOKUP_TABLE: &[u8; 58] =
    b"123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

// TODO make padding character configurable (now '1' following Bitcoin logic)
pub fn encode(reader: &mut dyn Read, writer: &mut dyn Write, padding: u8) -> Result<usize, Error> {
    let mut input: Vec<u8> = Vec::new();
    let n = reader.read_to_end(&mut input)?;
    // Source: https://github.com/bitcoin/bitcoin/blob/2068f089c8b7b90eb4557d3f67ea0f0ed2059a23/src/base58.cpp
    // `int size = (pend - pbegin) * 138 / 100 + 1; // log(256) / log(58), rounded up.`
    let mut b58: Vec<u8> = vec![0; n * 138 / 100 + 1];
    let mut length = 0usize;
    for i in 0..n {
        let mut carry = input[i] as u32;
        let mut j = 0usize;
        while j < b58.len() && (j < length || carry != 0) {
            let index = b58.len() - 1 - j;
            carry += 256 * (b58[index] as u32);
            b58[index] = (carry % 58) as u8;
            debug_assert!(b58[index] < 58);
            carry /= 58;
            j += 1;
        }
        debug_assert!(carry == 0);
        length = j;
    }
    let mut output: Vec<u8> = Vec::new();
    let mut index = 0usize;
    for i in index..b58.len() {
        if b58[i] != 0 {
            index = i;
            break;
        }
        output.push(padding);
    }
    for i in index..b58.len() {
        output.push(BASE58_LOOKUP_TABLE[b58[i] as usize]);
    }
    return writer.write(&output);
}

pub fn decode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut input: Vec<u8> = Vec::new();
    let n = reader.read_to_end(&mut input)?;
    let mut b256: Vec<u8> = vec![0; n * 733 / 1000 + 1];
    let mut length = 0usize;
    for i in 0..input.len() {
        let val = lookup_value(input[i]);
        if val.is_none() {
            return Err(Error::from(ErrorKind::InvalidInput));
        }
        let mut carry = val.unwrap() as u32;
        let mut j = 0usize;
        while j < b256.len() && (j < length || carry != 0) {
            let index = b256.len() - 1 - j;
            carry += 58 * (b256[index] as u32);
            b256[index] = (carry % 256) as u8;
            carry /= 256;
            j += 1;
        }
        debug_assert!(carry == 0);
        length = j;
    }
    let mut output: Vec<u8> = Vec::new();
    let mut index = 0usize;
    for i in index..b256.len() {
        // FIXME this loop is useless as zeroes are recreated in result?
        if b256[i] != 0 {
            index = i;
            break;
        }
    }
    for i in index..b256.len() {
        output.push(b256[i]);
    }
    return writer.write(&output);
}

fn lookup_value(c: u8) -> Option<u8> {
    match c {
        b'1' => Some(0),
        b'2' => Some(1),
        b'3' => Some(2),
        b'4' => Some(3),
        b'5' => Some(4),
        b'6' => Some(5),
        b'7' => Some(6),
        b'8' => Some(7),
        b'9' => Some(8),
        b'A' => Some(9),
        b'B' => Some(10),
        b'C' => Some(11),
        b'D' => Some(12),
        b'E' => Some(13),
        b'F' => Some(14),
        b'G' => Some(15),
        b'H' => Some(16),
        b'J' => Some(17),
        b'K' => Some(18),
        b'L' => Some(19),
        b'M' => Some(20),
        b'N' => Some(21),
        b'P' => Some(22),
        b'Q' => Some(23),
        b'R' => Some(24),
        b'S' => Some(25),
        b'T' => Some(26),
        b'U' => Some(27),
        b'V' => Some(28),
        b'W' => Some(29),
        b'X' => Some(30),
        b'Y' => Some(31),
        b'Z' => Some(32),
        b'a' => Some(33),
        b'b' => Some(34),
        b'c' => Some(35),
        b'd' => Some(36),
        b'e' => Some(37),
        b'f' => Some(38),
        b'g' => Some(39),
        b'h' => Some(40),
        b'i' => Some(41),
        b'j' => Some(42),
        b'k' => Some(43),
        b'm' => Some(44),
        b'n' => Some(45),
        b'o' => Some(46),
        b'p' => Some(47),
        b'q' => Some(48),
        b'r' => Some(49),
        b's' => Some(50),
        b't' => Some(51),
        b'u' => Some(52),
        b'v' => Some(53),
        b'w' => Some(54),
        b'x' => Some(55),
        b'y' => Some(56),
        b'z' => Some(57),
        _ => panic!("Illegal character."),
    }
}

#[cfg(test)]
mod tests {

    use super::{decode, encode};

    #[test]
    fn test_base58_encode_empty() {
        let mut input: &[u8] = &[];
        let mut output: Vec<u8> = Vec::new();
        assert_eq!(2, encode(&mut input, &mut output, b'1').unwrap());
        assert_eq!(b"11", &output[..]);
    }

    #[test]
    fn test_base58_decode_empty() {
        let mut input: &[u8] = &[];
        let mut output: Vec<u8> = Vec::new();
        // FIXME review if results are correct for empty string (or should empty encoded string be '11' padded)
        assert_eq!(1, decode(&mut input, &mut output).unwrap());
        assert_eq!(&[0;1], &output[..]);
    }

    #[test]
    fn test_base58_encode_sample_1() {
        let mut input: &[u8] = &[0, 0, 0, 1, 2, 3];
        let mut output: Vec<u8> = Vec::new();
        assert_eq!(9, encode(&mut input, &mut output, b'1').unwrap());
        assert_eq!(b"111111Ldp", &output[..]);
    }

    #[test]
    fn test_base58_decode_sample_1() {
        let expected: &[u8] = &[1, 2, 3];
        let mut input: &[u8] = b"111111Ldp";
        let mut output: Vec<u8> = Vec::new();
        assert_eq!(3, decode(&mut input, &mut output).unwrap());
        assert_eq!(expected, &output[..]);
    }

    #[test]
    fn test_base58_encode_sample_2() {
        let mut input: &[u8] = b"Test data";
        let mut output: Vec<u8> = Vec::new();
        assert_eq!(13, encode(&mut input, &mut output, b'1').unwrap());
        assert_eq!(b"25JnwSn7XKfNQ", &output[..]);
    }

    #[test]
    fn test_base58_decode_sample_2() {
        let expected: &[u8] = b"Test data";
        let mut input: &[u8] = b"25JnwSn7XKfNQ";
        let mut output: Vec<u8> = Vec::new();
        assert_eq!(9, decode(&mut input, &mut output).unwrap());
        assert_eq!(expected, &output[..]);
    }
}
