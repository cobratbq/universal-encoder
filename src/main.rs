mod base16;
mod base32;
mod base58;
mod base64;
mod base85;
mod common;
mod integer;
mod quoted_printable;
mod yenc;
// mod bencode;
mod dnsstamp;

use std::io::ErrorKind;

use common::Endianness;
use common::Signedness;

use structopt::StructOpt;

// FIXME consider not panicking on invalid characters.
// FIXME what to do with trimming trailing spaces/LF/CR?
fn main() {
    let stdin = std::io::stdin();
    let stdout = std::io::stdout();
    let opts = Opt::from_args();
    let result = match opts.encoding {
        Encoding::Base16 => base16::process(opts.decode, &mut stdin.lock(), &mut stdout.lock()),
        Encoding::Base32 => base32::process(opts.decode, &mut stdin.lock(), &mut stdout.lock()),
        Encoding::Base58 => base58::process(opts.decode, &mut stdin.lock(), &mut stdout.lock()),
        Encoding::Base64 => base64::process(opts.decode, &mut stdin.lock(), &mut stdout.lock()),
        Encoding::Base85 => base85::process(opts.decode, &mut stdin.lock(), &mut stdout.lock()),
        Encoding::Bencode => {
            todo!("To be implemented");
            // bencode::process(opts.decode, &mut stdin.lock(), &mut stdout.lock());
        }
        Encoding::DNSStamp => dnsstamp::process(opts.decode, &mut stdin.lock(), &mut stdout.lock()),
        Encoding::Integer {
            signedness,
            radix,
            endianness,
        } => integer::process(
            opts.decode,
            &mut stdin.lock(),
            &mut stdout.lock(),
            signedness,
            radix,
            endianness,
        ),
        Encoding::QuotedPrintable => quoted_printable::process(opts.decode, &mut stdin.lock(), &mut stdout.lock()),
        Encoding::Yenc { name, size } => yenc::process(
            opts.decode,
            &mut stdin.lock(),
            &mut stdout.lock(),
            name,
            size,
        ),
    };
    match result {
        // FIXME we no longer care about the amount of bytes outputted? does it make sense to try and keep exact length:usize?
        Ok(_) => {},
        Err(err) => {
            match err.kind() {
                ErrorKind::Unsupported => {
                    panic!("Operation is not supported.")
                },
                _ => {
                    panic!("Error: {}", err.to_string())
                },
            }
        }
    }
}

#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(short, long)]
    decode: bool,

    #[structopt(subcommand)]
    encoding: Encoding,
}

#[derive(StructOpt, Debug)]
enum Encoding {
    Base16,
    Base32,
    Base58,
    Base64,
    Base85,
    Bencode,
    DNSStamp,
    Integer {
        #[structopt(long, default_value = "unsigned")]
        signedness: Signedness,

        #[structopt(long, default_value = "10")]
        radix: u32,

        #[structopt(long, default_value = "big-endian")]
        endianness: Endianness,
    },
    QuotedPrintable,
    Yenc {
        #[structopt(long)]
        name: String,

        #[structopt(long)]
        size: usize,
    },
}
