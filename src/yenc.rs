use crc::{crc32, Hasher32};
use regex::Regex;
use std::io::{BufRead, BufReader, Error, ErrorKind, Read, Write};
use lazy_static::lazy_static;

const YENC_ESCAPE: u8 = b'=';
const YENC_CR: u8 = b'\r';
const YENC_LF: u8 = b'\n';
const YENC_LINE_LENGTH: usize = 128;
const YENC_NULL: u8 = 0;

lazy_static! {
    // FIXME make it possible to accept parameters in arbitrary positions, except 'name' is always last.
    // `=ybegin part=1 total=1 line=128 size=128000 name=my-file.bin\r\n`
    static ref YENC_HEAD_PATTERN: Regex = Regex::new(r"=ybegin part=(\d+) total=(\d+) line=(\d+) size=(\d+) name=(.+)").unwrap();
    // FIXME make it possible to accept parameters in arbitrary positions, except 'name' is always last.
    // `=yend size={} part=1 pcrc32={:x} crc32={:x}\r\n`
    static ref YENC_TAIL_PATTERN: Regex = Regex::new(r"=yend size=(\d+) part=(\d+) pcrc32=([0-9a-f]{8}) crc32=([0-9a-f]{8})").unwrap();
}

pub fn process(
    decoding: bool,
    reader: &mut dyn Read,
    writer: &mut dyn Write,
    filename: String,
    size: usize,
) -> Result<usize, Error> {
    if decoding {
        decode(reader, writer, filename, size)
    } else {
        encode(reader, writer, filename, size)
    }
}

// TODO: support for partitioning into multiple messages.
pub fn encode(
    reader: &mut dyn Read,
    writer: &mut dyn Write,
    filename: String,
    size: usize,
) -> Result<usize, Error> {
    let mut original_size: usize = 0;
    let mut encoded_size: usize = 0;
    let mut buffer: [u8; YENC_LINE_LENGTH] = [0; YENC_LINE_LENGTH];
    let mut encoded: Vec<u8> = Vec::new();
    let mut digest_full = crc32::Digest::new(crc32::IEEE);
    encoded_size += writer.write(
        format!(
            "=ybegin part=1 total=1 line={} size={} name={}\r\n",
            YENC_LINE_LENGTH, size, filename
        )
        .as_bytes(),
    )?;
    loop {
        encoded.clear();
        let n = reader.read(&mut buffer)?;
        if n == 0 {
            break;
        }
        original_size += n;
        &buffer[0..n].iter().for_each(|c| {
            let v = ((*c as u16 + 42) % 256) as u8;
            match v {
                c @ YENC_NULL | c @ YENC_LF | c @ YENC_CR | c @ YENC_ESCAPE => {
                    encoded.push(YENC_ESCAPE);
                    encoded.push(c + 64);
                }
                _ => encoded.push(v),
            }
        });
        encoded.push(YENC_CR);
        encoded.push(YENC_LF);
        digest_full.write(&encoded);
        encoded_size += writer.write(&encoded)?;
    }
    assert!(size == original_size);
    let crc32sum_full = digest_full.sum32();
    encoded_size += writer.write(
        format!(
            "=yend size={} part=1 pcrc32={:x} crc32={:x}\r\n",
            original_size, crc32sum_full, crc32sum_full
        )
        .as_bytes(),
    )?;
    writer.flush()?;
    let stderr = std::io::stderr();
    let mut reporter = stderr.lock();
    reporter
        .write(format!("Subject: [comment1] \"{}\" yEnc (1/1) [comment2]", filename).as_bytes())?;
    reporter.flush()?;
    Ok(encoded_size)
}

// TODO consider what kind of characters we should allow/trim in between yenc parts
// TODO make regexes lazy-init + static
// FIXME 'filename' parameter in order to override prescribed filename in yenc parts
// FIXME 'size' to verify that size matches with yenc parts (or zero to ignore?)
// FIXME handle optional header/footer parameters
pub fn decode(
    reader: &mut dyn Read,
    writer: &mut dyn Write,
    filename: String,
    size: usize,
) -> Result<usize, Error> {
    let mut bufreader = BufReader::new(reader);
    let mut line = String::new();
    let mut part = 0u16;
    let mut total = 0u16;
    let mut size = 0usize;
    let mut name = String::new();
    loop {
        if bufreader.read_line(&mut line)? == 0 {
            return Err(Error::from(ErrorKind::UnexpectedEof));
        }
        let caps = match YENC_HEAD_PATTERN.captures(line.as_str()) {
            None => return Err(Error::from(ErrorKind::InvalidInput)),
            Some(caps) => caps,
        };
        part = match u16::from_str_radix(&caps[1], 10) {
            Err(err) => return Err(Error::new(ErrorKind::InvalidInput, err)),
            Ok(val) => {
                if val != part + 1 {
                    return Err(Error::from(ErrorKind::InvalidInput));
                }
                val
            }
        };
        total = match u16::from_str_radix(&caps[2], 10) {
            Err(err) => return Err(Error::new(ErrorKind::InvalidInput, err)),
            Ok(val) => {
                if total > 0 && total != val {
                    return Err(Error::from(ErrorKind::InvalidInput));
                }
                val
            }
        };
        match u16::from_str_radix(&caps[3], 10) {
            Err(err) => return Err(Error::new(ErrorKind::InvalidInput, err)),
            Ok(val) => {
                println!("Line width: {}", val);
            }
        };
        size = match usize::from_str_radix(&caps[4], 10) {
            Err(err) => return Err(Error::new(ErrorKind::InvalidInput, err)),
            Ok(val) => {
                if size > 0 && size != val {
                    return Err(Error::from(ErrorKind::InvalidInput));
                }
                val
            }
        };
        name = caps[5].to_owned();
        // FIXME perform name verification against previous encountered name

        // FIXME read body of yenc message

        if bufreader.read_line(&mut line)? == 0 {
            return Err(Error::from(ErrorKind::UnexpectedEof));
        }
        let caps = match YENC_TAIL_PATTERN.captures(line.as_str()) {
            None => return Err(Error::from(ErrorKind::InvalidInput)),
            Some(caps) => caps,
        };
        // - size == previously read size / size in header
        match usize::from_str_radix(&caps[1], 10) {
            Err(err) => return Err(Error::new(ErrorKind::InvalidInput, err)),
            Ok(val) => {
                if val != size {
                    return Err(Error::from(ErrorKind::InvalidInput));
                }
            }
        }
        // - part == part index previously read in header
        match u16::from_str_radix(&caps[2], 10) {
            Err(err) => return Err(Error::new(ErrorKind::InvalidInput, err)),
            Ok(val) => {
                if val != part {
                    return Err(Error::from(ErrorKind::InvalidInput));
                }
            }
        }

        // FIXME process parameters:
        // - pcrc32 == crc32 for just read body content
        // - crc32 == crc32 for whole original file, should match with previously processed parts
    }
    // for-all yenc blocks in stdin
    // - read header
    //   - ensure filename is consistently repeated for each entry. (assign if unavailable, compare if available)
    //   - ensure part starts with 1, monotonically increasing, no missing parts.
    // - read body
    //   - ensure (max?) line length correct
    //   - ensure size matches with read body content
    // - read footer
    //   - ensure pcrc32 is correct for read body
    //   - ensure crc32 is consistently repeated over parts
    //   - ensure size consistent over parts
    // check part/total match up and all parts were encountered.
    // ensure crc32 matches original file content.
    // ensure size matches original file content.
    Err(Error::from(ErrorKind::Other))
}

#[cfg(test)]
mod tests {}
