use std::io::{Error, Read, Write};

use crate::base16::BASE16_LOOKUP_TABLE;

pub fn process(
    decoding: bool,
    reader: &mut dyn Read,
    writer: &mut dyn Write,
) -> Result<usize, Error> {
    if decoding {
        panic!("To be implemented.")
    } else {
        encode(reader, writer)
    }
}

pub fn encode(reader: &mut dyn Read, writer: &mut dyn Write) -> Result<usize, Error> {
    let mut size: usize = 0;
    let mut buffer: [u8; 1] = [0; 1];
    let mut encoder = WrappingWriter {
        writer: writer,
        count: 0,
    };
    loop {
        if reader.read(&mut buffer)? == 0 {
            break;
        }
        size += encoder.write(&buffer)?;
    }
    Ok(size)
}

struct WrappingWriter<'a> {
    writer: &'a mut dyn Write,
    count: usize,
}

impl WrappingWriter<'_> {
    fn write(&mut self, input: &[u8]) -> std::io::Result<usize> {
        let mut encoded: Vec<u8> = Vec::new();
        self.writer.write(match input.len() {
            1 => {
                if quoted_printable_accepted_character(input[0]) {
                    if self.count > 74 {
                        quoted_printable_push_soft_break(&mut encoded);
                        self.count = 0;
                    }
                    encoded.push(input[0]);
                    self.count += 1;
                } else {
                    if self.count > 72 {
                        quoted_printable_push_soft_break(&mut encoded);
                        self.count = 0;
                    }
                    self.count += quoted_printable_push_escaped(&mut encoded, input[0]);
                }
                &encoded
            }
            _ => panic!("Illegal number of bytes encountered."),
        })
    }
}

fn quoted_printable_push_escaped(encoded: &mut Vec<u8>, symbol: u8) -> usize {
    encoded.push(b'=');
    // Quoted-Printable encoding requires capital letters for hexadecimal representations.
    encoded.push(BASE16_LOOKUP_TABLE[((symbol & 0xf0) >> 4) as usize]);
    encoded.push(BASE16_LOOKUP_TABLE[(symbol & 0x0f) as usize]);
    3
}

fn quoted_printable_push_soft_break(encoded: &mut Vec<u8>) {
    encoded.push(b'=');
    encoded.push(b'\r');
    encoded.push(b'\n');
}

fn quoted_printable_accepted_character(c: u8) -> bool {
    c == b' ' || c == b'\t' || (c >= 33 && c <= 60) || (c >= 62 && c <= 126)
}

#[cfg(test)]
mod tests {

    use super::encode;

    #[test]
    fn test_quoted_printable_encoding_zero_length_string() {
        let expected: [u8; 0] = [0; 0];
        let mut input: &[u8] = &[0; 0];
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected, output.as_slice());
    }

    #[test]
    fn test_quoted_printable_encoding_wikipedia_example() {
        let expected = String::from("J'interdis aux marchands de vanter trop leur marchandises. Car ils se font =\r\nvite p=C3=A9dagogues et t'enseignent comme but ce qui n'est par essence qu'=\r\nun moyen, et te trompant ainsi sur la route =C3=A0 suivre les voil=C3=A0 bi=\r\nent=C3=B4t qui te d=C3=A9gradent, car si leur musique est vulgaire ils te f=\r\nabriquent pour te la vendre une =C3=A2me vulgaire.");
        let mut content = String::from("J'interdis aux marchands de vanter trop leur marchandises. Car ils se font vite pédagogues et t'enseignent comme but ce qui n'est par essence qu'un moyen, et te trompant ainsi sur la route à suivre les voilà bientôt qui te dégradent, car si leur musique est vulgaire ils te fabriquent pour te la vendre une âme vulgaire.").into_bytes();
        let mut input: &[u8] = content.as_mut();
        let mut output = vec![0; 0];
        let result = encode(&mut input, &mut output).unwrap();
        assert_eq!(expected.len(), result);
        assert_eq!(expected.as_bytes(), output.as_slice());
    }
}
