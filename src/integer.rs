use crate::common::Endianness;
use crate::common::Signedness;
use std::io::{Error, ErrorKind};
use std::io::{Read, Write};

use num_bigint::{BigInt, BigUint};
use num_traits::Num;

// TODO consider handling fixed-width output, padded with zeroes / error on space lacking.
pub fn process(
    decoding: bool,
    reader: &mut dyn Read,
    writer: &mut dyn Write,
    signedness: Signedness,
    radix: u32,
    endianness: Endianness,
) -> Result<usize, Error> {
    if decoding {
        decode(reader, writer, signedness, radix, endianness)
    } else {
        encode(reader, writer, signedness, radix, endianness)
    }
}

pub fn decode(
    reader: &mut dyn Read,
    writer: &mut dyn Write,
    signedness: Signedness,
    radix: u32,
    endianness: Endianness,
) -> Result<usize, Error> {
    let mut val: Vec<u8> = Vec::new();
    reader.read_to_end(&mut val)?;
    let converted = match signedness {
        Signedness::Unsigned => match endianness {
            Endianness::BigEndian => BigUint::from_bytes_be(&val),
            Endianness::LittleEndian => BigUint::from_bytes_le(&val),
        }
        .to_str_radix(radix),
        Signedness::Signed => match endianness {
            Endianness::BigEndian => BigInt::from_signed_bytes_be(&val),
            Endianness::LittleEndian => BigInt::from_signed_bytes_le(&val),
        }
        .to_str_radix(radix),
    };
    writer.write(converted.as_bytes())
}

pub fn encode(
    reader: &mut dyn Read,
    writer: &mut dyn Write,
    signedness: Signedness,
    radix: u32,
    endianness: Endianness,
) -> Result<usize, Error> {
    let mut val: Vec<u8> = Vec::new();
    reader.read_to_end(&mut val)?;
    let text: String = match String::from_utf8(val) {
        Ok(content) => content,
        Err(err) => return Err(Error::new(ErrorKind::InvalidInput, err)),
    };
    let converted = match signedness {
        Signedness::Unsigned => BigUint::from_str_radix(text.as_str(), radix)
            .map_err(|err| Error::new(ErrorKind::InvalidInput, err))
            .map(|val| match endianness {
                Endianness::BigEndian => val.to_bytes_be(),
                Endianness::LittleEndian => val.to_bytes_le(),
            }),
        Signedness::Signed => BigInt::from_str_radix(text.as_str(), radix)
            .map_err(|err| Error::new(ErrorKind::InvalidInput, err))
            .map(|val| match endianness {
                Endianness::BigEndian => val.to_signed_bytes_be(),
                Endianness::LittleEndian => val.to_signed_bytes_le(),
            }),
    }?;
    writer.write(&converted)
}
